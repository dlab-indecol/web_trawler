Changelog
=========

0.2.0 (2017-07-13)
------------------

* Added interactive options: -i to ask users whether or not to include files linked to from certain
  pages, and -I to ask users about whether to download each of the files found.

0.1.3 (2017-06-28)
------------------

* Removed external dependencies (lxml and cssselect)

0.1.2 (2017-06-22)
------------------

* Added Matlab example to readme
* Made functions accept number strings

0.1.1 (2017-06-21)
------------------

* Corrected package structuring mistake

0.1.0 (2017-06-21)
------------------

* Initial release
