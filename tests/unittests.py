import collections
import http.server
import os
import threading
import time
import urllib.error

import pytest

import web_trawler


Link = collections.namedtuple("Link", "href mb type")

get_links_expected = [
    Link(href='http://localhost:8000/tests/test_website/valid_link.html', mb='0.000', type='text/html'),
    Link(href='http://localhost:8000/tests/test_website/file.txt', mb='0.000', type='text/plain'),
    Link(href='http://localhost:8000/tests/test_website/file2.txt', mb='0.000', type='text/plain'),
    Link(href='http://localhost:8000/tests/test_website/manatee.jpg', mb='0.096', type='image/jpeg'),
    #Link(href='http://localhost:8000/tests/test_website/inexistant_file.txt',
    #     title="Here's a file that doesn't exists.",mb='?', type='?')
]

get_file_links_expected = [
    Link(href='http://localhost:8000/tests/test_website/file.txt', mb='0.000', type='text/plain'),
    Link(href='http://localhost:8000/tests/test_website/file2.txt', mb='0.000', type='text/plain'),
    Link(href='http://localhost:8000/tests/test_website/manatee.jpg', mb='0.096', type='image/jpeg')
]

get_file_links_from_linked_expected = [
    Link(href='http://localhost:8000/tests/test_website/file.txt', mb='0.000', type='text/plain'),
    Link(href='http://localhost:8000/tests/test_website/file2.txt', mb='0.000', type='text/plain'),
    Link(href='http://localhost:8000/tests/test_website/manatee.jpg', mb='0.096', type='image/jpeg'),
    Link(href='http://localhost:8000/tests/test_website/file3.txt', mb='0.000', type='text/plain')
]


@pytest.fixture(scope="module", autouse=True)
def web_server():
    httpd = http.server.HTTPServer(("", 8000), http.server.SimpleHTTPRequestHandler)
    server_thread = threading.Thread(target=httpd.serve_forever)
    server_thread.start()
    time.sleep(0.5)
    yield # above is setup, below is teardown
    httpd.shutdown()


def cleanup_download_folder():
    for f in os.listdir("download"):
        os.remove("download/" + f)
    os.rmdir("download")


def test_get_links_valid_url():
    returned = web_trawler.get_links("localhost:8000/tests/test_website")
    print(returned)
    assert returned == get_links_expected


def test_get_links_invalid_url():
    assert web_trawler.get_links("gorm's place") == []


def test_download_single_url():
    web_trawler.download("localhost:8000/tests/test_website/file.txt")
    file_downloaded = os.path.isfile("download/file.txt")
    cleanup_download_folder()
    assert file_downloaded


def test_download_url_list():
    web_trawler.download(["localhost:8000/tests/test_website/file.txt", "localhost:8000/tests/test_website/file2.txt"])
    files_downloaded = os.path.isfile("download/file.txt") and os.path.isfile("download/file2.txt")
    cleanup_download_folder()
    assert files_downloaded


def test_download_single_namedtuple():
    web_trawler.download(Link(href='http://localhost:8000/tests/test_website/file.txt',
                              mb='0.000', type='text/plain'))
    file_downloaded = os.path.isfile("download/file.txt")
    cleanup_download_folder()
    assert file_downloaded


def test_download_namedtuple_list():
    web_trawler.download([
        Link(href='http://localhost:8000/tests/test_website/file.txt',
             mb='0.000', type='text/plain'),
        Link(href='http://localhost:8000/tests/test_website/file2.txt',
             mb='0.000', type='text/plain')
    ])
    files_downloaded = os.path.isfile("download/file.txt") and os.path.isfile("download/file2.txt")
    cleanup_download_folder()
    assert files_downloaded


def test_download_invalid_source():
    with pytest.raises(urllib.error.URLError):
        web_trawler.download("gorm's place")


def test_download_target_xpath():
    web_trawler.download("localhost:8000/tests/test_website/file.txt", target="../web_trawler/tests/download")
    file_downloaded = os.path.isfile("tests/download/file.txt")
    cleanup_download_folder()
    assert file_downloaded


def test_download_max_mb_set():
    web_trawler.download(Link(href='http://localhost:8000/tests/test_website/manatee.jpg',
                              mb='0.094', type='image'), mb_per_file_limit=0.001)
    file_downloaded = os.path.isfile("download/manatee.jpg") == False
    cleanup_download_folder()
    assert file_downloaded


def test_get_file_links_valid_url():
    returned = web_trawler.get_file_links("localhost:8000/tests/test_website")
    assert set(returned) == set(get_file_links_expected)


def test_get_file_links_invalid_url():
    assert web_trawler.get_file_links("localhost:8000/tests/test_website/invalid") == []


def test_get_file_links_max_files_set():
    with pytest.raises(RuntimeError):
        web_trawler.get_file_links("localhost:8000/tests/test_website", no_of_files_limit=1)


def test_get_file_links_no_of_processes_settings():
    one = web_trawler.get_file_links("localhost:8000/tests/test_website", add_links_from_linked_pages=True,
                                     no_of_processes_specified_by_user=1)
    ten = web_trawler.get_file_links("localhost:8000/tests/test_website", add_links_from_linked_pages=True,
                                     no_of_processes_specified_by_user=10)

    assert set(one) == set(get_file_links_from_linked_expected)
    assert set(ten) == set(get_file_links_from_linked_expected)
    with pytest.raises(ValueError):
        web_trawler.get_file_links("localhost:8000/tests/test_website", add_links_from_linked_pages=True,
                                   no_of_processes_specified_by_user=0)


def test_get_file_links_whitelist_blacklist():
    returned = web_trawler.get_file_links("localhost:8000/tests/test_website", whitelist="jpg", blacklist=["xls", "txt"])
    assert returned == [Link(href='http://localhost:8000/tests/test_website/manatee.jpg',
                             mb='0.096', type='image/jpeg')]
